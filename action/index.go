package action

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Login(c *gin.Context) {
	c.HTML(http.StatusOK, "login", gin.H{"title": "ログイン", "message": ""})
}

func Error(c *gin.Context) {
	c.JSON(http.StatusInternalServerError, gin.H{"result": "error"})
}
