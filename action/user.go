package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tsuchina.ga/practice/hello-go/domain/user"
	"net/http"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func GetUsersPage(c *gin.Context) {
	if users, err := user.FindAll(); err != nil {
		c.HTML(http.StatusOK, "users/list", gin.H{"title": "ユーザ一覧", "message": err.Error(), "users": nil})
	} else {
		c.HTML(http.StatusOK, "users/list", gin.H{"title": "ユーザ一覧", "message": "", "users": users})
	}
}

func GetUsers(c *gin.Context) {
	if users, err := user.FindAll(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"users": users})
	}
}

func GetUserPage(c *gin.Context) {
	if e, err := user.FindById(c.Param("id")); err != nil {
		c.HTML(http.StatusOK, "users/detail", gin.H{"title": "ユーザ詳細", "message": err.Error(), "user": nil})
	} else {
		c.HTML(http.StatusOK, "users/detail", gin.H{"title": "ユーザ詳細", "message": "", "user": e})
	}
}

func GetUser(c *gin.Context) {
	if e, err := user.FindById(c.Param("id")); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"user": e})
	}
}

func AddUserPage(c *gin.Context) {
	c.HTML(http.StatusOK, "users/add", gin.H{"title": "ユーザ追加", "message": ""})
}

func AddUser(c *gin.Context) {
	var e user.User

	if err := c.Bind(&e); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	e.Id = bson.NewObjectId()
	e.CreatedAt = time.Now()
	e.PasswordToHash()
	if err := user.Insert(e); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"success": "success"})
}

func UpdateUserPage(c *gin.Context) {
	if e, err := user.FindById(c.Param("id")); err != nil {
		c.HTML(http.StatusOK, "users/update", gin.H{"title": "ユーザ更新", "message": err.Error(), "user": nil})
	} else {
		c.HTML(http.StatusOK, "users/update", gin.H{"title": "ユーザ更新", "message": "", "user": e})
	}
}

func UpdateUser(c *gin.Context) {
	id := c.Param("id")
	org, err := user.FindById(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var e user.User
	if err := c.Bind(&e); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//mapping
	org.Name = e.Name
	org.Email = e.Email
	if e.Password != "" {
		org.Password = e.Password
		org.PasswordToHash()
	}

	if err := user.Update(id, org); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"success": "success"})
}

func DeleteUserPage(c *gin.Context) {
	id := c.Param("id")

	m := ""
	e, err := user.FindById(id)
	if err != nil {
		m = err.Error()
	}

	c.HTML(http.StatusOK, "users/delete", gin.H{"title": "ユーザ削除", "message": m, "user": e})
}

func DeleteUser(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "parameter error"})
		return
	}

	if err := user.Delete(id); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"success": "success"})
}
