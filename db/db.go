package db

import (
	"gopkg.in/mgo.v2"
	"log"
	"os"
)

var session *mgo.Session
var isConnected bool

const (
	address  = "hello-db"
	database = "hello"
	username = "hello"
	password = "hello"
)

func New() *mgo.Session {
	if !isConnected {
		isConnected = true

		s, err := mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    []string{address},
			Database: database,
			Username: username,
			Password: password,
		})

		if err != nil {
			log.Println(err)
			os.Exit(-1)
		}

		session = s
	}

	return session.Clone()
}
