package user

import (
	"gitlab.com/tsuchina.ga/practice/hello-go/db"
	"gopkg.in/mgo.v2/bson"
)

const (
	d = "hello"
	c = "users"
)

func FindAll() (users []User, err error) {

	s := db.New()
	defer s.Close()

	err = s.DB(d).C(c).Find(bson.M{}).All(&users)
	return
}

func FindById(id string) (e User, err error) {

	s := db.New()
	defer s.Close()

	err = s.DB(d).C(c).Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&e)
	return
}

func Insert(e User) (err error) {

	s := db.New()
	defer s.Close()

	return s.DB(d).C(c).Insert(e)
}

func Update(id string, e User) (err error) {

	s := db.New()
	defer s.Close()

	return s.DB(d).C(c).UpdateId(bson.ObjectIdHex(id), e)
}


func Delete(id string) (err error) {

	s := db.New()
	defer s.Close()

	return s.DB(d).C(c).RemoveId(bson.ObjectIdHex(id))
}
