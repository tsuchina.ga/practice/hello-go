package user

import (
	"time"
	"crypto/hmac"
	"crypto/sha256"
	"gopkg.in/mgo.v2/bson"
	"net/url"
	"encoding/base64"
)

type User struct {
	Id        bson.ObjectId `bson:"_id" json:"id"`
	Name      string        `bson:"name" json:"name,omitempty"`
	Email     string        `bson:"email" json:"email,omitempty"`
	LoginId   string        `bson:"login_id" json:"login_id,omitempty"`
	Password  string        `bson:"password" json:"-"`
	IsValid   bool          `bson:"is_valid" json:"is_valid,omitempty"`
	CreatedAt time.Time     `bson:"created_at" json:"created_at,omitempty"`
}

func (user *User) PasswordToHash() {
	hash := hmac.New(sha256.New, []byte(user.LoginId))
	hash.Write([]byte(user.Password))
	user.Password = url.QueryEscape(base64.StdEncoding.EncodeToString(hash.Sum(nil)))
}
