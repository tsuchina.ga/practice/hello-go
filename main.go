package main

import (
	"github.com/gin-gonic/gin"
	"github.com/foolin/gin-template"
	"log"
	"time"
	"html/template"
	"gitlab.com/tsuchina.ga/practice/hello-go/action"
	"os"
	"io"
)

const (
	logPath   = "logs/"
	noticeLog = logPath + "notice.log"
	errorLog  = logPath + "error.log"
)

func main() {
	// logger
	n, err := os.OpenFile(noticeLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		n, _ = os.Create(noticeLog)
	}

	e, err := os.OpenFile(errorLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		e, _ = os.Create(errorLog)
	}

	gin.DefaultWriter = io.MultiWriter(n)
	gin.DefaultErrorWriter = io.MultiWriter(e)
	log.SetOutput(gin.DefaultWriter)

	r := gin.Default()

	// template
	r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
		Root: "template",
		Extension: ".html",
		Master: "include/master",
		Funcs: template.FuncMap{
			"add": func(a, b int) int { return a + b },
			"sub": func(a, b int) int { return a - b },
			"mul": func(a, b int) int { return a * b },
			"div": func(a, b int) int { return a / b },
			"date": func(a time.Time) string { return a.Format("2006-01-02") },
			"timeIsZero": func(a time.Time) bool { return a.IsZero() },
			"today": func() string { return time.Now().Format("2006-01-02") },
		},
		DisableCache: false,
	})

	//static
	r.StaticFile("/robots.txt", "./template/static/robots.txt")
	r.Static("/css", "./template/static/css")
	r.Static("/js", "./template/static/js")

	//routing
	r.GET("/", action.Login)
	r.GET("/login", action.Login)
	r.GET("/error", action.Error)

	//users page
	r.GET("/users", action.GetUsersPage)
	r.GET("/users/detail/:id", action.GetUserPage)
	r.GET("/users/add", action.AddUserPage)
	r.GET("/users/update/:id", action.UpdateUserPage)
	r.GET("/users/delete/:id", action.DeleteUserPage)

	//users api
	r.GET("/api/users", action.GetUsers)
	r.GET("/api/users/:id", action.GetUser)
	r.POST("/api/users", action.AddUser)
	r.PUT("/api/users/:id", action.UpdateUser)
	r.DELETE("/api/users/:id", action.DeleteUser)

	log.Fatal(r.Run(":9000"))
}
